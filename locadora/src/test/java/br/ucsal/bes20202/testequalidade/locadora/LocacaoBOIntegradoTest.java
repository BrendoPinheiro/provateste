package br.ucsal.bes20202.testequalidade.locadora;
//
///**
// * Testes INTEGRADOS para os métodos da classe LocacaoBO.
// * 
// * @author claudioneiva
// *
// */
//public class LocacaoBOIntegradoTest {
//
//	/**
//	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
//	 * fabricaçao e 3 veículos com 8 anos de fabricação.
//	 */
//	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
//	}
//
//}

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20202.testequalidade.builder.ClienteBuilder;
import br.ucsal.bes20202.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20202.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.exception.CampoObrigatorioNaoInformado;
import br.ucsal.bes20202.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.exception.LocacaoNaoEncontradaException;
import br.ucsal.bes20202.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20202.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20202.testequalidade.locadora.persistence.VeiculoDAO;
import br.ucsal.bes20202.testequalidade.locadora.util.ValidadorUtil;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */

	public static ClienteBuilder builderCliente;
	public static VeiculoBuilder builderVeiculo;
	
	private VeiculoDAO veiculoDAO = new VeiculoDAO();
	private LocacaoBO locacaoBO = new LocacaoBO(veiculoDAO);

	@BeforeAll
	public static void setup() {	
		builderCliente = ClienteBuilder.clienteDefault();
		builderVeiculo = VeiculoBuilder.veiculoDefault();
	}
	
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws ClienteNaoEncontradoException, VeiculoNaoEncontradoException, LocacaoNaoEncontradaException {
		Cliente cliente = builderCliente.but().withCPF("234.233.411-40")
											   .withNome("Sicrano da Silva")
											   .withTelefone("71939492392")
											   .build();
		ClienteDAO.insert(cliente);
		cliente = ClienteDAO.obterPorCpf(cliente.getCpf());
		Veiculo veiculo1 = builderVeiculo.but().build();
		Veiculo veiculo2 = builderVeiculo.but().withPlaca("SAK-URA1").withAno(2019).withValorDiaria(100.0).build();
		Veiculo veiculo3 = builderVeiculo.but().withPlaca("NAR-UTO2").withAno(2015).withValorDiaria(60.0).build();
		Veiculo veiculo4 = builderVeiculo.but().withPlaca("SAS-UKE3").withAno(2014).withValorDiaria(50.0).build();
		Veiculo veiculo5 = builderVeiculo.but().withPlaca("ITA-CHI4").withAno(2020).withValorDiaria(120.0).build();
		veiculoDAO.insert(veiculo1);
		veiculoDAO.insert(veiculo2);
		veiculoDAO.insert(veiculo3);
		veiculoDAO.insert(veiculo4);
		veiculoDAO.insert(veiculo5);
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(veiculoDAO.obterPorPlaca(veiculo1.getPlaca()));
		veiculos.add(veiculoDAO.obterPorPlaca(veiculo2.getPlaca()));
		veiculos.add(veiculoDAO.obterPorPlaca(veiculo3.getPlaca()));
		veiculos.add(veiculoDAO.obterPorPlaca(veiculo4.getPlaca()));
		veiculos.add(veiculoDAO.obterPorPlaca(veiculo5.getPlaca()));
		Locacao locacao = new Locacao(cliente,veiculos,new Date(),3);
		LocacaoDAO.insert(locacao);
		locacao = LocacaoDAO.obterPorNumeroContrato(locacao.getNumeroContrato());
	    Double totalEsperado = 1056.0;
	    
	    List<String> placas = new ArrayList<String>();
	    placas.add(veiculo1.getPlaca());
	    placas.add(veiculo2.getPlaca());
	    placas.add(veiculo3.getPlaca());
	    placas.add(veiculo4.getPlaca());
	    placas.add(veiculo5.getPlaca());
	    
	    Double totalAtual = locacaoBO.calcularValorTotalLocacao(placas, 
	    		locacao.getQuantidadeDiasLocacao(), locacao.getDataLocacao().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		assertEquals(totalEsperado,totalAtual);
	}
	
	@Test
	public void testarExceptions() {
	 assertThrows(ClienteNaoEncontradoException.class, ()-> {
		 ClienteDAO.obterPorCpf("9999");
	 });
	 
	 assertThrows(VeiculoNaoEncontradoException.class, ()-> {
		 veiculoDAO.obterPorPlaca("293-9999");
	 });
	 
	 assertThrows(LocacaoNaoEncontradaException.class, ()-> {
		 LocacaoDAO.obterPorNumeroContrato(90);
	 });
	 
	 assertThrows(CampoObrigatorioNaoInformado.class, ()-> {
		 Date data = null;
		 ValidadorUtil.validarCampoObrigatorio(data, "data");
	 });
	 
	 assertThrows(CampoObrigatorioNaoInformado.class, ()-> {
		 Integer numero = null;
		 ValidadorUtil.validarCampoObrigatorio(numero, "numero");
	 });
	
	 assertThrows(CampoObrigatorioNaoInformado.class, ()-> {
		 List lista = null;
		 ValidadorUtil.validarCampoObrigatorio(lista, "lista");
	 });
	}


}

