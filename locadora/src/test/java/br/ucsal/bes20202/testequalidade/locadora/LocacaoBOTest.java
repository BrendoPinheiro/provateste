package br.ucsal.bes20202.testequalidade.locadora;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Any;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.mockito.ArgumentMatchers.*;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;

import br.ucsal.bes20202.testequalidade.builder.ClienteBuilder;
import br.ucsal.bes20202.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20202.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.exception.LocacaoNaoEncontradaException;
import br.ucsal.bes20202.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20202.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20202.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes UNITÁRIOS para os métodos da classe LocacaoBO (utilize o MOCKITO).
 * 
 * @author claudioneiva
 *
 */

@RunWith(MockitoJUnitRunner.class)
@PrepareForTest({ LocacaoBO.class, ClienteDAO.class, VeiculoDAO.class, LocacaoDAO.class })
public class LocacaoBOTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */

	public static VeiculoBuilder builderVeiculo;
	public static ClienteBuilder builderCliente;

//	private static LocacaoDAO locacaoDAO = new LocacaoDAO();
	
	@Mock
	private static VeiculoDAO veiculoDAOFake;
	private static LocacaoDAO locacaoDAO;
	private static ClienteDAO clienteDAO;

	private static LocacaoBO locacaoBO;

	public static Cliente cliente;
	public static Veiculo veiculo;
	public static List<String> placas;
	public static List<Veiculo> veiculos;
	public static Locacao locacao;
	

	@BeforeAll
	public void setup()
			throws ClienteNaoEncontradoException, VeiculoNaoEncontradoException, LocacaoNaoEncontradaException {
		MockitoAnnotations.initMocks(this);
		Cliente cliente = builderCliente.but().withCPF("234.233.411-40").withNome("Sicrano da Silva")
				.withTelefone("71939492392").build();
		ClienteDAO.insert(cliente);
		cliente = ClienteDAO.obterPorCpf(cliente.getCpf());
		Veiculo veiculo1 = builderVeiculo.but().build();
		Veiculo veiculo2 = builderVeiculo.but().withPlaca("ORO-CHI1").withAno(2019).withValorDiaria(100.0).build();
		Veiculo veiculo3 = builderVeiculo.but().withPlaca("NAR-UTO2").withAno(2015).withValorDiaria(60.0).build();
		Veiculo veiculo4 = builderVeiculo.but().withPlaca("SAS-UKE3").withAno(2014).withValorDiaria(50.0).build();
		Veiculo veiculo5 = builderVeiculo.but().withPlaca("ITA-CHI4").withAno(2020).withValorDiaria(120.0).build();
		
//		.withPlaca("SAK-URA1").withAno(2019).withValorDiaria(100.0)
		
		List<Veiculo> veiculos = new ArrayList<>();
		
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);
		
		placas.add("0KP-2938");
		placas.add("ORO-CHI1");
		placas.add("NAR-UTO2");
		placas.add("SAS-UKE3");
		placas.add("ITA-CHI4");
		
		veiculoDAOFake = new VeiculoDAO();
		Mockito.when(veiculoDAOFake.obterPorPlacas(any())).thenReturn(veiculos);
		
		locacaoBO = new LocacaoBO(veiculoDAOFake);
		

	}

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws Exception {

		LocalDate data = LocalDate.now();
		
		Double totalEsperado = 1056.0;
		Double totalAtual = locacaoBO.calcularValorTotalLocacao(placas, 3, data);
		assertEquals(totalEsperado, totalAtual);
				

	}

}
