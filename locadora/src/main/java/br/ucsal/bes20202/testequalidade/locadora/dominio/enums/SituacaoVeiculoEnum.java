package br.ucsal.bes20202.testequalidade.locadora.dominio.enums;

public enum SituacaoVeiculoEnum {
	DISPONIVEL, MANUTENCAO, LOCADO;
}
